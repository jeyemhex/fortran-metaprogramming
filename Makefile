#
# Makefile
# Edward Higgins, 2020-04-15 15:58
#

FC     = gfortran
CFLAGS = -Wall -O0 -fcheck=all -g
LFLAGS = -ldl
all: metaprogramming_example

vpath %.f90 src/

metaprogramming_example: metaprogramming_example.o meta.o

metaprogramming_example.o: meta.o

meta.o:

%:%.o
	$(FC) -o $@ $^ $(CFLAGS) $(LFLAGS) 

%.o: %.f90
	$(FC) -c $< $(CFLAGS)

clean:
	rm -f metaprogramming_example *.o *.mod

# vim:ft=make
#
