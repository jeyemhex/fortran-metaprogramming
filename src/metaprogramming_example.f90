program metaprogramming_example
!==============================================================================#
! METAPROGRAMMING_EXAMPLE
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2020-04-15
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use iso_c_binding
  use meta
  implicit none

  abstract interface
    function operate(a, b) result(c)
      real :: c
      real, intent(in)  :: a, b
    end function operate
  end interface

  procedure(operate), pointer :: add => null()

  type(c_ptr)    :: handle
  type(c_funptr) :: c_func

  ! Define and build the function
  character(len=128) :: add_src = "function add(a, b) result(c) ; real :: a, b, c; c = a + b; end function add"
  call meta_build(add_src)

  ! Open the newly created library and import the `add` function
  handle = meta_open()
  c_func = meta_load("add", handle)
  call c_f_procpointer(c_func, add)

  ! Try calling our new add function
  print *, add(2.0, 5.0)

  ! Close the library
  call meta_close(handle)

end program metaprogramming_example
