module meta
!==============================================================================#
! META
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2020-04-15
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use iso_c_binding
  implicit none

  private

  integer(kind=c_int), parameter :: RTLD_LAZY = 1 ! Lazy function call binding.  */
  integer(kind=c_int), parameter :: RTLD_NOW  = 2 ! Immediate function call binding.  */

  public :: meta_build, meta_open, meta_close, meta_load

  interface
    function dlopen(fname, mode) bind(c)
      use iso_c_binding
      type(c_ptr)            :: dlopen
      character(kind=c_char) :: fname(*)
      integer(kind=c_int),value    :: mode
    end function dlopen

    function dlerror() bind(c)
      use iso_c_binding
      type(c_ptr) :: dlerror
    end function dlerror

    function dlsym(handle, proc_name) bind(c)
      use iso_c_binding
      type(c_funptr)         :: dlsym
      type(c_ptr), value     :: handle
      character(kind=c_char) :: proc_name(*)
    end function dlsym

    function dlclose(handle) bind(c)
      use iso_c_binding
      integer(kind=c_int)    :: dlclose
      type(c_ptr)            :: handle
    end function dlclose
  end interface

contains

  ! Builds some fortran source into a dynamic library
  !   (currently stored in /tmp/meta_code.so)
  subroutine meta_build(src)
    character(len=*), intent(in)  :: src

    open(11, file="/tmp/meta_code.f90", status='new')
    write(11,*) src
    flush(11)
    close(11)

    call execute_command_line("gfortran -g -shared /tmp/meta_code.f90 -o /tmp/meta_code.so -fPIC")
  end subroutine meta_build

  ! Opens a dynamic library so that we can access its procedures
  function meta_open() result(handle)
    type(c_ptr) :: handle

    character(kind=c_char), pointer :: error(:)

    handle = dlopen("/tmp/meta_code.so", RTLD_NOW);
    if (.not. c_associated(handle)) then
      call c_f_pointer(dlerror(), error, [80])
      print *, error
      error stop
    end if

  end function meta_open

  ! Loads a named procedure from an open dynamic library
  function meta_load(proc_name, handle) result(c_func)
    type(c_funptr)                :: c_func
    character(len=*), intent(in)  :: proc_name
    type(c_ptr),      intent(in)  :: handle

    character(kind=c_char), pointer :: error(:)

    c_func = dlsym(handle, trim(proc_name) // "_" // achar(0))
    call c_f_pointer(dlerror(), error, [80])
    if (associated(error)) then
      print *, error
      error stop
    end if

  end function meta_load

  ! Closes an open dynamic library
  subroutine meta_close(handle)
    type(c_ptr), intent(inout) :: handle

    integer(kind=c_int)           :: ierr
    character(kind=c_char), pointer :: error(:)

    ierr = dlclose(handle)
    call c_f_pointer(dlerror(), error, [80])
    if (associated(error)) then
      print *, error
      error stop
    end if

  end subroutine meta_close 

end module meta
